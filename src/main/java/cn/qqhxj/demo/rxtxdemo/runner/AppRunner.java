package cn.qqhxj.demo.rxtxdemo.runner;

import cn.qqhxj.demo.rxtxdemo.dataobject.BiochemistryDO;
import cn.qqhxj.demo.rxtxdemo.mapper.BiochemistryMapper;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
@Slf4j
public class AppRunner implements ApplicationRunner {
    @Resource
    private BiochemistryMapper biochemistryMapper;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<BiochemistryDO> biochemistryDOS = biochemistryMapper.selectList(null);
        log.info("{}", JSONObject.toJSONString(biochemistryDOS));
    }
}
