package cn.qqhxj.demo.rxtxdemo;

import cn.qqhxj.demo.rxtxdemo.dataobject.BiochemistryDO;
import cn.qqhxj.demo.rxtxdemo.mapper.BiochemistryMapper;
import cn.qqhxj.rxtx.context.SerialContext;
import cn.qqhxj.rxtx.parse.SerialDataParser;
import cn.qqhxj.rxtx.processor.SerialDataProcessor;
import cn.qqhxj.rxtx.starter.annotation.EnableSerialPort;
import cn.qqhxj.rxtx.starter.annotation.EnableSerialPortAutoConfig;
import cn.qqhxj.rxtx.starter.annotation.SerialPortBinder;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
@EnableSerialPortAutoConfig
@Slf4j
//@EnableSerialPort(value = "COM1", port = "COM1")
@EnableSerialPort(value = "COM3", port = "COM3")
public class SericalServer implements SerialDataProcessor<byte[]> {

    private static final byte ACK = 0x06;

    private static final byte LF = 0x0a;

    private static final byte CR = 0x0d;

    private static final byte STX = 0x02;

    private static final byte EOT = 0x04;

    private static final byte ENQ = 0x05;

    private static final byte NAK = 0x15;

    private static final byte ETX = 0x03;


    @Resource
    private BiochemistryMapper biochemistryMapper;

    private BiochemistryDO biochemistryDO;


    @Lazy
    @Resource
    @Qualifier("COM3.SerialContext")
    private SerialContext serialContext;

    @Override
    public void process(byte[] bytes, SerialContext serialContext) {
        log.info("message received = {}", bytes);
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stxStr = new StringBuilder();
        for (byte aByte : bytes) {
            if(aByte == LF){
                stringBuilder.append("<LF>");
                stxStr.append("<LF>");
                continue;
            }
            if (aByte == CR){
                stringBuilder.append("<CR>");
                stxStr.append("<CR>");
                continue;
            }
            if(aByte == STX){
                stringBuilder.append("<STX>");
                stxStr = new StringBuilder();
                stxStr.append("<STX>");
                continue;
            }
            if(aByte == EOT){
                stringBuilder.append("<EOT>");
                biochemistryDO.setTenantId(1L);
                log.info("DO:{}", JSONObject.toJSONString(biochemistryDO));
                biochemistryMapper.insert(biochemistryDO);
                biochemistryDO = new BiochemistryDO();
                continue;
            }
            if(aByte == ENQ){
                stringBuilder.append("<ENQ>");
                biochemistryDO = new BiochemistryDO();
                continue;
            }
            if(aByte == NAK){
                stringBuilder.append("<NAK>");
                continue;
            }
            if(aByte == ETX){
                stringBuilder.append("<ETX>");
                stxStr.append("<ETX>");
                continue;
            }

            char c = (char) aByte;
            stringBuilder.append(c);
            stxStr.append(c);

        }

        log.info("str:{}",stringBuilder);

        String strString = stxStr.toString();
        log.info("{}",strString);
        if(strString.contains("O<CR>")){
            List<String> strings = stxToList(strString,"\\^");
            log.info("strings:{}",strings);
            if(biochemistryDO.getHosBarcode() != null){
                biochemistryDO.setTenantId(1L);
                biochemistryMapper.insert(biochemistryDO);
                biochemistryDO = new BiochemistryDO();
            }
            String s = strings.get(0);
            String hosBarcode = s.substring(s.lastIndexOf("|") + 1);
            log.info("hosBarcode:{}",hosBarcode);
            biochemistryDO.setHosBarcode(hosBarcode);
        }

        //名字都是写死的,一个一个判断
        if(strString.contains("^^^ALT|")){
            String stxValue = getStxValue("^^^ALT|", strString);
            biochemistryDO.setAlanineAminotransferase(stxValue);
        }


        if(strString.contains("^^^AST|")){
            String stxValue = getStxValue("^^^AST|", strString);
            biochemistryDO.setAspartateAminotransferase(stxValue);
        }

        if(strString.contains("^^^TP|")){
            String stxValue = getStxValue("^^^TP|", strString);
            biochemistryDO.setTp(stxValue);
        }

        if(strString.contains("^^^ALB|")){
            String stxValue = getStxValue("^^^ALB|", strString);
            biochemistryDO.setAlbumin(stxValue);
        }

        if(strString.contains("^^^TBIL|")){
            String stxValue = getStxValue("^^^TBIL|", strString);
            biochemistryDO.setTotalBilirubin(stxValue);
        }

        if(strString.contains("^^^DBIL|")){
            String stxValue = getStxValue("^^^DBIL|", strString);
            biochemistryDO.setDirectBilirubin(stxValue);
        }

        if(strString.contains("^^^GGT|")){
            String stxValue = getStxValue("^^^GGT|", strString);
            biochemistryDO.setGgt(stxValue);
        }

        if(strString.contains("^^^ALP|")){
            String stxValue = getStxValue("^^^ALP|", strString);
            biochemistryDO.setAlp(stxValue);
        }

        if(strString.contains("^^^TBA|")){
            String stxValue = getStxValue("^^^TBA|", strString);
            biochemistryDO.setTba(stxValue);
        }

        if(strString.contains("^^^BUN|")){
            String stxValue = getStxValue("^^^BUN|", strString);
            biochemistryDO.setUrea(stxValue);
        }

        if(strString.contains("^^^CRE|")){
            String stxValue = getStxValue("^^^CRE|", strString);
            biochemistryDO.setCreatinine(stxValue);
        }

        if(strString.contains("^^^UA|")){
            String stxValue = getStxValue("^^^UA|", strString);
            biochemistryDO.setUa(stxValue);
        }

        if(strString.contains("^^^GLU|")){
            String stxValue = getStxValue("^^^GLU|", strString);
            //葡萄糖赋值给空腹血糖
            biochemistryDO.setFastingBloodSugar(stxValue);
        }

        if(strString.contains("^^^TG|")){
            String stxValue = getStxValue("^^^TG|", strString);
            biochemistryDO.setTriglyceride(stxValue);
        }

        if(strString.contains("^^^TC|")){
            String stxValue = getStxValue("^^^TC|", strString);
            biochemistryDO.setTotalCholesterol(stxValue);
        }



        //处理消息 这里做了一个回显
        serialContext.sendData(new byte[]{ACK});
    }
//
//    @SerialPortBinder("COM1")
//    @Bean
//    public SerialDataParser serialDataParser() {
//        return new ByteSerialDataParser();
//    }

    @SerialPortBinder("COM3")
    @Bean
    public SerialDataParser serialDataParser2() {
        return new ByteSerialDataParser();
    }

    private List<String> stxToList(String stxStr,String splitFlag){
        String[] split = stxStr.split(splitFlag);
        List<String> list = new ArrayList<>();
        for (String s : split) {
            if(!s.isEmpty()){
                list.add(s);
            }
        }
        return list;
    }

    private String getStxValue(String findStr,String stxStr){
        int i = stxStr.indexOf(findStr) + findStr.length();
        return stxStr.substring(i, stxStr.indexOf("|", i));
    }
}
