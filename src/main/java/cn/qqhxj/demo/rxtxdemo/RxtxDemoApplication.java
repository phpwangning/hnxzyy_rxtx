package cn.qqhxj.demo.rxtxdemo;


import cn.qqhxj.rxtx.context.SerialContext;
import cn.qqhxj.rxtx.parse.HexStringSerialDataParser;
import cn.qqhxj.rxtx.parse.SerialDataParser;
import cn.qqhxj.rxtx.parse.StringSerialDataParser;
import cn.qqhxj.rxtx.processor.SerialDataProcessor;
import cn.qqhxj.rxtx.starter.annotation.EnableSerialPort;
import cn.qqhxj.rxtx.starter.annotation.EnableSerialPortAutoConfig;
import cn.qqhxj.rxtx.starter.annotation.SerialPortBinder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

import javax.annotation.Resource;

//@EnableSerialPortAutoConfig
@Slf4j
@SpringBootApplication
//@SerialPortBinder("COM1")
//@EnableSerialPort(value = "COM1", port = "COM1")
//@EnableSerialPort(value = "COM3", port = "COM3")
public class RxtxDemoApplication {



    public static void main(String[] args) {
        SpringApplication.run(RxtxDemoApplication.class, args);
    }

}

