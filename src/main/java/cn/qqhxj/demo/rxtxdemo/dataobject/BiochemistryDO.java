package cn.qqhxj.demo.rxtxdemo.dataobject;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 医疗数据-生化数据(MdBiochemistry)表实体类
 *
 * @author makejava
 * @since 2024-08-30 11:02:25
 */
@Data
@TableName("md_biochemistry")
public class BiochemistryDO {

    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 病例id
     */
    private Long patientId;
    /**
     * 外部条形码（体检编号）
     */
    private String hosBarcode;
    /**
     * 建档人名称
     */
    private String authorizorName;
    /**
     * 建档人id
     */
    private String authorizor;
    /**
     * 条形码
     */
    private String barCode;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 核验人
     */
    private String inspector;
    /**
     * 核验人姓名
     */
    private String inspectorName;
    /**
     * 患者姓名
     */
    private String patientName;
    /**
     * 套餐编号
     */
    private String applyCode;
    /**
     * 套餐名称
     */
    private String applyitemName;
    /**
     * 检查医生id
     */
    private String examinationDoctorId;
    /**
     * 空腹血糖(mmol/L)
     */
    private String fastingBloodSugar;
    /**
     * 空腹血糖mg/dL
     */
    private String fastingBloodSugarOtherExpress;
    /**
     * 直接胆红素(μ mol/L)
     */
    private String directBilirubin;
    /**
     * 白蛋白(g/L)
     */
    private String albumin;
    /**
     * 丙氨酸氨基转移酶(U/L)
     */
    private String alanineAminotransferase;
    /**
     * 天门冬氨酸氨基转移酶(U/L)
     */
    private String aspartateAminotransferase;
    /**
     * 肌酐(μ mol/L)
     */
    private String creatinine;
    /**
     * 尿素(mmol/L)
     */
    private String urea;
    /**
     * 总胆固醇(mmol/L)S
     */
    private String totalCholesterol;
    /**
     * 总胆红素(μ mol/L)
     */
    private String totalBilirubin;
    /**
     * 甘油三酯(mmol/L)
     */
    private String triglyceride;
    /**
     * 低密度脂蛋白胆固醇(mmol/L)
     */
    private String ldlCholesterol;
    /**
     * 高密度脂蛋白胆固醇(mmol/L)
     */
    private String hdlCholesterol;
    /**
     * 谷草/谷丙比值
     */
    private String astAlt;
    /**
     * 白细胞计数(10^9/L)
     */
    private String leukocyteCount;
    /**
     * 淋巴细胞百分数(%)
     */
    private String lym;
    /**
     * 中性粒细胞百分数(%)
     */
    private String neutrophilPercentage;
    /**
     * 淋巴细胞绝对值(10^9/L)
     */
    private String lymphocytesAbsoluteValue;
    /**
     * 中性粒细胞绝对值(10^9/L)
     */
    private String neutrophilsAbsoluteValue;
    /**
     * 红细胞计数(10^12/L)
     */
    private String rbcCount;
    /**
     * 血红蛋白量(g/L)
     */
    private String hemoglobinContent;
    /**
     * 红细胞比积(%)
     */
    private String hematocrit;
    /**
     * 平均红细胞体积(fL)
     */
    private String hematocritAverageVolume;
    /**
     * 平均红细胞血红蛋白量(pg)
     */
    private String meanCorpuscularHemoglobin;
    /**
     * 平均红细胞血红蛋白浓度
     */
    private String mchc;
    /**
     * 红细胞体积分布宽度(%)
     */
    private String rdwCv;
    /**
     * 血小板计数（PLT）(10^9/L)
     */
    private String plt;
    /**
     * 血小板压积
     */
    private String pct;
    /**
     * 平均血小板体积(fL)
     */
    private String mpv;
    /**
     * 血小板体积分布宽度(fl)
     */
    private String pvdw;
    /**
     * 单核细胞绝对值(10^9/L)
     */
    private String monocytesBsoluteValue;
    /**
     * 单核细胞百分数(%)
     */
    private String monocytesPercentage;
    /**
     * 嗜酸性粒细胞绝对值(10^9/L)
     */
    private String eosinophilsAbsoluteValue;
    /**
     * 嗜碱性粒细胞绝对值(10^9/L)
     */
    private String basophilicAbsoluteValue;
    /**
     * 嗜碱性粒细胞百分数(%)
     */
    private String eosinophilsPercentage;
    /**
     * 嗜酸性粒细胞百分数(%)
     */
    private String basophilicPercentage;
    /**
     * 尿酸(umolL)
     */
    private String ua;
    /**
     * 血钠浓度（mmol/L）【5 浮点数值】【8】
     */
    private String na;
    /**
     * 葡萄糖:(mmo/L)
     */
    private String glu;
    /**
     * 钾:(mmol/L)
     */
    private String k;
    /**
     * 谷氨酰转移酶
     */
    private String ggt;

    /**
     * 碱性磷酸酶
     */
    private String alp;

    /**
     * 胆汁酸
     */
    private String tba;

    /**
     * 总蛋白
     */
    private String tp;
    /**
     * 上报状态 0-未上报 1-上报成功 2-上报失败
     */
    private Integer reportStatus;

    private Long tenantId;
}

