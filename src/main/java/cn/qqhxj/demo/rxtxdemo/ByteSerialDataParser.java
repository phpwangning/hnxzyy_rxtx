package cn.qqhxj.demo.rxtxdemo;

import cn.qqhxj.rxtx.context.SerialContext;
import cn.qqhxj.rxtx.parse.SerialDataParser;

public class ByteSerialDataParser  implements SerialDataParser<byte[]> {
    @Override
    public byte[] parse(byte[] bytes, SerialContext serialContext) {
        return bytes;
    }
}
