package cn.qqhxj.demo.rxtxdemo.mapper;

import cn.qqhxj.demo.rxtxdemo.dataobject.BiochemistryDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BiochemistryMapper extends BaseMapper<BiochemistryDO> {
}
